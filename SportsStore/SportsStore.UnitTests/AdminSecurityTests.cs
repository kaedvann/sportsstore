﻿using Moq;
using NUnit.Framework;
using SportsStore.WebUI.Controllers;
using SportsStore.WebUI.Infrastructure.Abstract;
using SportsStore.WebUI.Models;
using System.Web.Mvc;

namespace SportsStore.UnitTests
{
    [TestFixture]
    public class AdminSecurityTests
    {
        [Test]
        public void Can_Login_With_Valid_Credentials()
        {
            // Arrange - create a mock authentication provider
            var mock = new Mock<IAuthProvider>();
            mock.Setup(m => m.Authenticate("admin", "secret")).Returns(true);

            // Arrange - create the view model
            var model = new LoginViewModel
            {
                UserName = "admin",
                Password = "secret"
            };

            // Arrange - create the controller
            var target = new AccountController(mock.Object);

            // Act - authenticate using valid credentials
            var result = target.Login(model, "/MyURL");

            // Assert
            Assert.IsInstanceOf(typeof (RedirectResult), result);
            Assert.AreEqual("/MyURL", ((RedirectResult) result).Url);
        }

        [Test]
        public void Cannot_Login_With_Invalid_Credentials()
        {
            // Arrange - create a mock authentication provider
            var mock = new Mock<IAuthProvider>();
            mock.Setup(m => m.Authenticate("badUser", "badPass")).Returns(false);

            // Arrange - create the view model
            var model = new LoginViewModel
            {
                UserName = "badUser",
                Password = "badPass"
            };

            // Arrange - create the controller
            var target = new AccountController(mock.Object);

            // Act - authenticate using valid credentials
            var result = target.Login(model, "/MyURL");

            // Assert
            Assert.IsInstanceOf(typeof (ViewResult), result);
            Assert.IsFalse(((ViewResult) result).ViewData.ModelState.IsValid);
        }
    }
}