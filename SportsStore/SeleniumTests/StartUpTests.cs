﻿using NUnit.Framework;

namespace SeleniumTests
{
    [TestFixture]
    public class StartUpTests
    {
        [Test]
        public void TestBrowserOpening()
        {
            var driver = Browser.Driver;
            Assert.NotNull(driver);
            Assert.AreEqual(driver.Url, Browser.SiteUrl);
        }
    }
}
