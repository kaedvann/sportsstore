﻿using NUnit.Framework;
using OpenQA.Selenium;

namespace SeleniumTests
{
    [TestFixture]
    public class CartTests
    {
        [TearDown]
        public void CleanUp()
        {
            Browser.Driver.Refresh();
        }
        [Test]
        public void TestGoingToShipping()
        {
            var button = Browser.Driver.FindElement(By.CssSelector("[type=submit]"));
            button.Click();
            var checkoutButton = Browser.Driver.FindElementByCssSelector("a[href=\"/Cart/Checkout\"]");
            checkoutButton.Click();
            Assert.IsTrue(Browser.Driver.Url.Contains("/Cart/Checkout"));
        }
    }
}
