﻿using System.Globalization;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using SportsStore.Domain.Concrete;

namespace SeleniumTests
{
    [TestFixture]
    public class HomePageTests
    {
        [Test]
        public void TestShownItemsQuantity()
        {
            var context = new EFDbContext();
            var expected = context.Products.Count();
            var driver = Browser.Driver;
            var actual = driver.FindElementsByClassName("item").Count;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestShownCategoriesQuantity()
        {
            var context = new EFDbContext();
            var expected = context.Products.Select(p => p.Category).Distinct().Count() + 1; //так как "все" не зависит от количества категорий
            var driver = Browser.Driver;
            var actual = driver.FindElements(By.CssSelector("[title=category]")).Count;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestBuyingAll()
        {
            var context = new EFDbContext();
            for (int i = 0; i < 4; ++i)
            {
                var buttons = Browser.Driver.FindElements(By.CssSelector("[type=submit]"));
                buttons[i].Click();
                Browser.Driver.FindElementsByTagName("a").First(e => e.Text == "Continue shopping").Click();
            }
            var caption = Browser.Driver.FindElementByClassName("caption");
            var content = caption.Text;
            Assert.IsTrue(content.Contains("4 item"));
            var culture = CultureInfo.CreateSpecificCulture("en-US");
            var price = context.Products.Sum(p => p.Price);
            Assert.IsTrue(content.Contains(price.ToString("C", culture)));
        }

        [TearDown]
        public void CleanUp()
        {
            Browser.Driver.Refresh();
        }
    }
}
