﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;

namespace SeleniumTests
{
    /// <summary>
    /// Простой класс для хранения инстанса веб-драйвера между тестами и управления им
    /// </summary>
    public static class Browser
    {
        #region Закрытие консоли и окна
        private static readonly object Cleaner;
        private class Finalizer
        {
            private RemoteWebDriver _driverHandle;

            public Finalizer(RemoteWebDriver driver)
            {
                _driverHandle = driver;
            }

            ~Finalizer()
            {
                _driverHandle.Quit();
            }
        }
        #endregion
        static Browser()
        {
            _driver = new ChromeDriver(@"../../..");
            _driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(5));
            _driver.Navigate().GoToUrl(SiteUrl);
            Cleaner = new Finalizer(_driver);
        }



        public const string SiteUrl = @"http://localhost:" + @"61576/"; //@"E:\Coding\C#\sportsstore\SportsStore"
        private static readonly RemoteWebDriver _driver;
        public static RemoteWebDriver Driver { get { return _driver; } }

        public static void Refresh(this RemoteWebDriver driver)
        {
            driver.ExecuteScript("localStorage.clear();");
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Navigate().Refresh();
            driver.Navigate().GoToUrl(SiteUrl);
        }
    }
}
