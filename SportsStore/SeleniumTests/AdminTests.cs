﻿using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace SeleniumTests
{
    [TestFixture]
    public class AdminTests
    {
        [TearDown]
        public void Clear()
        {
            Browser.Driver.Refresh();
        }
        [Test]
        public void TestRedirectingToLogin()
        {
            Browser.Driver.Navigate().GoToUrl(Browser.SiteUrl + "Admin/Index");
            Assert.IsTrue(Browser.Driver.Url.Contains("/Account/Login"));
        }

        [Test]
        public void TestDoesntAcceptWrongPassword()
        {
            var driver = Browser.Driver;
            driver.Navigate().GoToUrl(Browser.SiteUrl + "Admin/Index");
            driver.FindElementById("UserName").SendKeys("admin");
            driver.FindElementById("Password").SendKeys("admin");
            driver.FindElementByCssSelector("[type=submit]").Click();
            Assert.IsTrue(driver.FindElementByClassName("validation-summary-errors").Displayed);
            ;
        }

        [Test]
        public void TestAcceptsPassword()
        {
            var driver = Browser.Driver;
            driver.Navigate().GoToUrl(Browser.SiteUrl + "Admin/Index");
            driver.FindElementById("UserName").SendKeys("admin");
            driver.FindElementById("Password").SendKeys("secret");
            driver.FindElementByCssSelector("[type=submit]").Click();
            Assert.IsTrue(Browser.Driver.Url.Contains("/Admin/"));
            
        }

        [TestCase]
        public void TestAddingItem()
        {
            var driver = Browser.Driver;
            driver.Navigate().GoToUrl(Browser.SiteUrl + "Admin/Index");
            driver.FindElementById("UserName").SendKeys("admin");
            driver.FindElementById("Password").SendKeys("secret");
            driver.FindElementByCssSelector("[type=submit]").Click();
            var addButton = driver.FindElementsByTagName("a").Last();
            addButton.Click();
            Assert.IsTrue(driver.Url.Contains("Create"));
            //var fields = driver.FindElementsByClassName("editor-field").ToList();
            foreach (var field in new[] { driver.FindElementById("Name"), driver.FindElementById("Description"), driver.FindElementById("Category") })
            {
                field.SendKeys("test");
            }
            driver.FindElementById("Price").Clear();
            driver.FindElementById("Price").SendKeys("2.00");
            driver.FindElementByCssSelector("[type=submit]").Click();
            driver.Navigate().GoToUrl(Browser.SiteUrl);
            Assert.AreEqual(2, driver.FindElementByClassName("pager").FindElements(By.TagName("a")).Count);
            // fields[2].SendKeys("2.00");
        }

        [TestCase]
        public void TestDeletingItem()
        {
            var driver = Browser.Driver;
            driver.Navigate().GoToUrl(Browser.SiteUrl + "Admin/Index");
            driver.FindElementById("UserName").SendKeys("admin");
            driver.FindElementById("Password").SendKeys("secret");
            driver.FindElementByCssSelector("[type=submit]").Click();
            driver.FindElementsByCssSelector("[type=submit]").Last().Click();
            driver.Navigate().GoToUrl(Browser.SiteUrl);
            Assert.AreEqual(1, driver.FindElementByClassName("pager").FindElements(By.TagName("a")).Count);
           
        }
    }
}
