﻿using SportsStore.Domain.Entities;
using System.Data.Entity;

namespace SportsStore.Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        static EFDbContext() 
        {
            Database.SetInitializer(new EfConfig());
        }
        public DbSet<Product> Products { get; set; }
    }
}