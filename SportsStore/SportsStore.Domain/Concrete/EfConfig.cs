﻿using System.Data.Entity;
using SportsStore.Domain.Entities;

namespace SportsStore.Domain.Concrete
{
    public class EfConfig : DropCreateDatabaseIfModelChanges<EFDbContext>
    {
        protected override void Seed(EFDbContext context)
        {
            context.Products.Add(new Product
            {
                Category = "Skiing",
                Name = "Turbo skis",
                Price = 220,
                Description = "Superb skis for absolute speed"
            });
            context.Products.Add(new Product
            {
                Category = "Skiing",
                Name = "Skiing glasses",
                Price = 100,
                Description = "Yellow glasses for good vision"
            });
            context.Products.Add(new Product
            {
                Category = "Football",
                Name = "Old Trafford",
                Price = 90000000,
                Description = "One of the most famous stadiums in the world"
            });
            context.Products.Add(new Product
            {
                Category = "Swimming",
                Description = "Feel like a fish in water",
                Name = "Swimming trunks",
                Price = 88
            });
        }
    }
}